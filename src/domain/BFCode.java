package domain;

import java.io.File;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

import domain.enums.Structure;

public class BFCode {
	private File bfFile;
	private char[] actualCode;
	private long[] cells = new long[300000];
        private int currentCell = 0;

	private final static List<Character> allowedCharacters = new ArrayList<>(Arrays.asList('>', '<', '+', '-', '.', ',', '[', ']'));

	public BFCode(String fileName) {
		this.bfFile = new File(fileName);
		cleanupCode();
	}

	public void execute() {	
		for(int i = 0; i < actualCode.length; ) {
			switch(determineStructure(actualCode[i])) {
                        	        case SEQUENT_STRUCTURE:
                                	        handleSequentStructure(actualCode[i]);
                                		i++;
						break;
					case ITERATIVE_STRUCTURE:				
						int end = findCorrespondingLoop(i);
                                        	i = handleIterativeStructure(i, end);
		                           	break;
                        }
		}
	}

	private Structure determineStructure(char currentCommand) {
		if(currentCommand == '[') return Structure.ITERATIVE_STRUCTURE;
		else return Structure.SEQUENT_STRUCTURE;
	}

	private void handleSequentStructure(int currentCommand) {
                switch(currentCommand) {
                        case '>': currentCell++;
                                        break;
                        case '<': currentCell--;
                                        break;
                        case '+': cells[currentCell]++;
                                        break;
                        case '-': cells[currentCell]--;
                                        break;
                        case '.': System.out.print((char)cells[currentCell]);
                                        break;
                        case ',': cells[currentCell] = new Scanner(System.in).nextInt();
                                        break;
                }
        }

	private int handleIterativeStructure(int start, int end) {
		while(cells[currentCell] != 0) {
			for(int i = start + 1; i < end; ) {
				if(actualCode[i] == '[') {
					int innerEnd = findCorrespondingLoop(i);
					i = handleIterativeStructure(i, innerEnd);
				} else if(actualCode[i] != ']') {
					handleSequentStructure(actualCode[i]);
					i++;
				}
			}
		}
		return end + 1;
	}

	private int findCorrespondingLoop(int indexFrom) {
		Stack<Character> stack = new Stack<>();
		for(int i = indexFrom; i < actualCode.length; i++) {
			if(actualCode[i] == '[') {
				stack.push('[');
			} else if(actualCode[i] == ']') {
				stack.pop();
				if(stack.empty()) {
					return i;
				}
			}
		}
		return -1;
	}

	private void cleanupCode() {

		// TODO: split to readFromFile() than cleanup()

		try {
			StringBuilder actualCode = new StringBuilder();
			BufferedReader reader = new BufferedReader(new FileReader(this.bfFile));
			int buffer;
			boolean commentBracketMet = true;
			buffer = reader.read();
			if((char)buffer == '[') commentBracketMet = false;
			else if(allowedCharacters.contains((char)buffer)) actualCode.append((char)buffer);
			while((buffer = reader.read()) != -1) {
					if(allowedCharacters.contains((char)buffer) && commentBracketMet) actualCode.append((char)buffer);
					else if((char)buffer == ']') commentBracketMet = true;
			}
			this.actualCode = actualCode.toString().toCharArray();
		} catch (IOException e) {
			System.out.println(bfFile.getPath());
			System.out.println("Bad filename, exiting");
			System.exit(-1);
		}
	}
}
