package domain.enums;

public enum Structure {
	SEQUENT_STRUCTURE,
	SELECTIVE_STRUCTURE,
	ITERATIVE_STRUCTURE
}
